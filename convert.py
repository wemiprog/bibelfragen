import json
import re

dic = {
    "gn": "1. Mose",
    "ex": "2. Mose",
    "lv": "3. Mose",
    "nm": "4. Mose",
    "dt": "5. Mose",
    "jo": "Josua",
    "jd": "Richter",
    "jg": "Richter",
    "rt": "Ruth",
    "1s": "1. Samuel",
    "2s": "2. Samuel",
    "1k": "1. Könige",
    "2k": "2. Könige",
    "ez": "Esra",
    "ne": "Nehemia",
    "es": "Esther",
    "pr": "Sprüche",
    "ec": "Prediger",
    "dn": "Daniel",
    "hs": "Hosea",
    "jl": "Joel",
    "am": "Amos",
    "ob": "Obadja",
    "jn": "Jona",
    "mc": "Micha",
    "nh": "Nahum",
    "hb": "Habakuk",
    "zp": "Zephanja",
    "hg": "Haggai",
    "ml": "Maleachi",
    "mt": "Matthäus",
    "mk": "Markus",
    "lk": "Lukas",
    "jh": "Johannes",
    "ac": "Apostelgeschichte",
    "rm": "Römer",
    "1c": "1. Korinther",
    "2c": "2. Korinther",
    "ep": "Epheser",
    "ph": "Philipper",
    "rv": "Offenbarung"
}

dic2 = {
    "gen": "1. Mose",
    "ex": "2. Mose",
    "lev": "3. Mose",
    "num": "4. Mose",
    "deut": "5. Mose",
    "josh": "Josua",
    "jud": "Richter",
    "judg": "Richter",
    "judges": "Richter",
    "ruth": "Ruth",
    "1 sam": "1. Samuel",
    "2 sam": "2. Samuel",
    "1 kgs": "1. Könige",
    "1 kings": "1. Könige",
    "2 kgs": "2. Könige",
    "2 kg": "2. Könige",
    "ezra": "Esra",
    " ezra": "Esra",
    "neh": "Nehemia",
    "est": "Esther",
    " est": "Esther",
    "es": "Esther",
    "prov": "Sprüche",
    "prvo": "Sprüche",
    "proverbs": "Sprüche",
    " prov": "Sprüche",
    "pov": "Sprüche",
    "pr": "Sprüche",
    "eccl": "Prediger",
    "dan": "Daniel",
    "hos": "Hosea",
    " hos": "Hosea",
    "joel": "Joel",
    "joes": "joel",
    "am": "Amos",
    "obad": "Obadja",
    "jn": "Jona",
    "mic": "Micha",
    "nah": "Nahum",
    "hab": "Habakuk",
    "zeph": "Zephanja",
    "hag": "Haggai",
    "mal": "Maleachi",
    "mat": "Matthäus",
    "mt": "Matthäus",
    "mk": "Markus",
    "m": "Markus",
    "lk": "Lukas",
    "jh": "Johannes",
    "acts": "Apostelgeschichte",
    "act": "Apostelgeschichte",
    "rom": "Römer",
    "1 cor": "1. Korinther",
    "2 cor": "2. Korinther",
    "eph": "Epheser",
    "php": "Philipper",
    "ph": "Philipper",
    "rev": "Offenbarung",
    "re": "Offenbarung",
    " rev": "Offenbarung"
}
all_questions = []
questions = []


def translate_bookname(text):
    wreg = "\([1-3]? ?[A-Za-z][a-z]*.? ?[0-9]{1,2}[:;,.][0-9]{1,2}\)"
    text_within = re.findall(wreg, text)
    if(len(text_within) > 0):
        text_within = text_within[0]
        book_old = re.findall("[1-3]? ?[A-Za-z][a-z]*", text_within)[0].lower()
        book_new = dic2[book_old]
        chapter_pre = re.findall("[a-z].? ?[0-9]{1,2}", text_within)[0]
        chapter = re.findall("[0-9]{1,2}", chapter_pre)[0]
        verse_pre = re.findall("[;:,.][0-9]{1,2}", text_within)[0]
        verse = re.findall("[0-9]{1,2}", verse_pre)[0]
        newTextWI = "(" + book_new + " " + chapter + ":" + verse + ")"
        text = re.sub(wreg, newTextWI, text)
    return text


def process_xml(xml_file):
    line_count = 0
    id_string = ""
    text_started = False
    answ_started = False
    this_question = {}

    for line in xml_file:
        # if line_count > 15:
            # print(line, end="")
        # if line_count > 1530:
        #    break

        # Question info start
        if("<QU " in line):
            # find id
            id_string = re.findall("QN=\"[0-9]+\"", line)
            id = id_string[0].replace("QN=\"", "").replace("\"", "")
            print(id + " ", end="")

            # find difficulty
            dif_string = re.findall("DL=\"[BbIA]{1}\"", line)
            dif_only = dif_string[0].replace("DL=\"", "").replace("\"", "")
            if(dif_only == "B" or dif_only == "b"):
                difficulty = 1
            elif (dif_only == "I"):
                difficulty = 2
            else:
                difficulty = 3

            # find book
            book_string = re.findall("BK=\"[a-zA-Z0-9][a-zA-Z]", line)
            book_only = book_string[0].replace("BK=\"", "").lower()
            book = dic[book_only]

            # find chapter, needs adaption for psalms
            chapter_string = re.findall("CH=\"[0-9]{1,2}", line)
            chapter = chapter_string[0].replace("CH=\"", "")

            # find correct answer
            answer_string = re.findall("CA=\"[A-E]{1}", line)
            answer = answer_string[0].replace("CA=\"", "").lower()
            print(answer)

            # start answer array
            answers = {}
        # question info stop

        # question text start
        if re.match("<QT>[^A-Z]", line) is not None:
            text_started = True
        elif text_started is True:
            text = line.replace("\n", "")
            question = translate_bookname(text)
            text_started = False
        elif ("<QT>" in line):
            text = line.replace("<QT>", "").replace(
                "</QT>", "").replace("\n", "")
            question = translate_bookname(text)
        # question text stop

        # answers start
        # if re.match("<AA>", line) is not None:
        if "<AA>\n" in line or "<AB>\n" in line or "<AC>\n" in line or "<AD>\n" in line or "<AE>\n" in line:
            answ_started = re.findall("[A-Z]{2}", line)[0][1:]
        elif answ_started:
            answers[answ_started] = line.replace("\n", "")
            answ_started = False
        elif "<AA>" in line or "<AB>" in line or "<AC>" in line or "<AD>" in line or "<AE>" in line:
            answer_key = re.findall("[A-Z]{2}", line)[0][1:]
            answer_holder = re.sub("</?[A-Z]{2}>", "", line)
            answers[answer_key] = answer_holder.replace("\n", "")
        # answers stop

        if "</QU>" in line:
            this_question["id"] = id
            this_question["frage"] = question
            this_question["schwierigkeit"] = difficulty
            this_question["buch"] = book
            this_question["kapitel"] = chapter
            this_question["korrekt"] = answer
            this_question["antworten"] = answers
            all_questions.append(this_question)
            this_question = {}

        line_count += 1


with open("fragen.xml") as xml_file:
    process_xml(xml_file)

with open("fragen_nt.xml") as xml_file:
    process_xml(xml_file)

with open('fragen.json', 'w') as outfile:
    json.dump(all_questions, outfile)
